import { Component, OnInit, OnDestroy } from '@angular/core';
import { FilmService} from '../film.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-actor-list',
  templateUrl: './actor-list.component.html',
  styleUrls: ['./actor-list.component.scss']
})
export class ActorListComponent implements OnInit, OnDestroy {
  /**
   * newActors, films - to save data after GET request
   * filteredActors - filtered array of actors
   * blinkTimer - to wait 0.5 sec before showing "Connection to server" to avoid blinking between film clicking
   * stickyFilms - stick film container when scrolling down
   * error - message from server or information about processes
   * actorValue - actor name in URL and to pass in input form
   */
  newActors: Array<String>;
  filteredActors: Array<String>;
  films: Array<Object>;
  blinkTimer: any;
  stickyFilms = false;
  error: string;
  actorValue: string;

  constructor(private filmService: FilmService) { }

  /**
   * check for actor as parameter in URL
   * set this actor in INPUT (%20 = leerspace)
   * update the actor list
   * add event listener on scroll (to fix film container)
   */
  ngOnInit() {
    const index = window.location.href.indexOf('=');
    const name = (index !== -1) ? window.location.href.slice(index + 1) : '';
    this.actorValue = name.replace(/%20/g, ' ');

    this.getActors();
    window.addEventListener('scroll', this.handleScroll);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  /**
   * GET list of actors with this name
   * set timer: only show "connecting" message if no fast response from server
   * SUCCESS: save the response and filter the array (if actor name was in URL)
   * ERROR: error message
   */
  getActors() {
    this.newActors = null;
    this.films = null;
    this.blinkTimer = setTimeout(() => {
      this.error = 'Connecting to server...';
    }, 500);

    this.filmService.getActors()
      .map(actors => actors.sort())
      .subscribe(
        actors => {
          clearTimeout(this.blinkTimer);
          this.newActors = actors;
          this.error = null;
          this.handleActorChange(this.actorValue);
          },
        error => {
          clearTimeout(this.blinkTimer);
          this.error = error;
        }
      );
  }

  /**
   * GET list of films with this actor
   * set timer: only show "connecting" message if no fast response from server
   * @param {String} name - actor name
   */
  getFilmsWithActor(name: String) {
    this.films = null;
    this.blinkTimer = setTimeout(() => {
      this.error = 'Connecting to server...';
    }, 500);
    this.filmService.getFilmsWithActor(name)
      .map(films => this.filmService.sortFilmList(films, 'title'))
      .subscribe(
        films => {
          clearTimeout(this.blinkTimer);
          this.error = null;
          this.films = films;
          },
        error => {
          clearTimeout(this.blinkTimer);
          this.error = error;
          }
        );
  }

  /**
   * Close the film panel
   * Filter the array of actors to pass the inputs value
   * if nothing found - set as null (for ngIf) and show error
   */
  handleActorChange(actorName) {
    this.films = null;
    this.error = null;
    this.filteredActors = this.newActors.filter(val => val.toLowerCase().includes(actorName));
    if (this.filteredActors.length === 0) {
      this.filteredActors = null;
      this.error = 'Nothing found';
    }
  }

  /**
   * scroll event to change sticky container with films if scrolled down
   */
  handleScroll = (): void => {
    const change: boolean = window.pageYOffset > 250;
    if (this.stickyFilms !== change) {
      this.stickyFilms = change;
    }
  }
}
