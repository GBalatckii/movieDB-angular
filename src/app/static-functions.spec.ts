import { checkInput } from './static-functions';

/**
 * Check static function checkInput
 * The "stringToSortedArray" function will be tested with check input from actor list
 */
describe('Static functions', () => {
  it('Titel works fine', () => {
    /* Check title */
    expect(checkInput('title', 'Normal title')).toEqual(true);
    expect(checkInput('title', 'BigTitleThatHasMoreThan50CharactersShouldNotPassThisTest')).toEqual(false);
  });

  it('Year works fine', () => {
    /* Check year*/
    const now = (new Date()).getFullYear();
    expect(checkInput('year', '1799')).toEqual(false);
    expect(checkInput('year', '1800')).toEqual(true);
    expect(checkInput('year', String(now))).toEqual(true);
    expect(checkInput('year', String(now + 1))).toEqual(false);
  });

  it('Synopsis works fine', () => {
    /* Check synopsis */
    expect(checkInput('desc', 'smallDescriptionWithLessThan200Characters')).toEqual(false);
  });

  it('Ratings work fine', () => {
    /* Check critics (same as users) */
    expect(checkInput('critics', '-1')).toEqual(false);
    expect(checkInput('critics', '0')).toEqual(true);
    expect(checkInput('critics', '100')).toEqual(true);
    expect(checkInput('critics', '101')).toEqual(false);
  });

  it('Poster link works fine', () => {
    /* Check poster link (test 10 links from random films) */
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BMjI2NjEzNjc4NV5BMl5BanBnXkFtZTgwODQxOTc5MDE@._V1_UY268_CR12,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BMTYzMzU4NDUwOF5BMl5BanBnXkFtZTcwMTM5MjA5Ng@@._V1_UX182_CR0,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BMTgxOTY4Mjc0MF5BMl5BanBnXkFtZTcwNTA4MDQyMw@@._V1_UY268_CR3,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BMTYzNDc2MDc0N15BMl5BanBnXkFtZTgwOTcwMDQ5MTE@._V1_UX182_CR0,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BMjIyZGU4YzUtNDkzYi00ZDRhLTljYzctYTMxMDQ4M2E0Y2YxXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_UX182_CR0,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BOTVhMWQ5MDktMGE3OS00MjVlLWExZWYtMzY2MTg4ZGFiZDQ5L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BMTc5NjY4MjUwNF5BMl5BanBnXkFtZTgwODM3NzM5MzE@._V1_UX182_CR0,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BMzU5MjEwMTg2Nl5BMl5BanBnXkFtZTcwNzM3MTYxNA@@._V1_UY268_CR0,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BODAzNDMxMzAxOV5BMl5BanBnXkFtZTgwMDMxMjA4MjE@._V1_UX182_CR0,0,182,268_AL_.jpg'))
      .toEqual(true);
    expect(checkInput('plink', 'https://images-na.ssl-images-amazon.com/images/M' +
      '/MV5BMjEyNTA3Njk4M15BMl5BanBnXkFtZTcwMzkzMzY3Mw@@._V1_UY268_CR2,0,182,268_AL_.jpg'))
      .toEqual(true);
  });

  it('Film link works fine', () => {
    /* Check film link */
    expect(checkInput('flink', '-1')).toEqual(false);
    expect(checkInput('flink', '0')).toEqual(false);
    expect(checkInput('flink', '1')).toEqual(true);
    expect(checkInput('flink', '9999999')).toEqual(true);
    expect(checkInput('flink', '10000000')).toEqual(false);
  });

  it('Actor list works fine', () => {
    /* Check actors */
    expect(checkInput('actorlist', 'Less Than, Five Actors, Should Not, Pass This Test')).toEqual(false);
    expect(checkInput('actorlist', 'Wr§ong Symb$ols, Mu%st No&t, Fa?il This T!est, I*f We Have, F%$§ive Actors ')).toEqual(true);
    expect(checkInput('actorlist', '   Check ,  For  ,  Multiply ,   Spaces ,   To Pass   ')).toEqual(true);
    expect(checkInput('actorlist', ',Multiply,, Commas, Should Not,, ,, Fail, This Test,,,')).toEqual(true);
    expect(checkInput('actorlist', 'AlL CaSEs, ShOUld, UNForTunalY, PASS, ThIS TesT')).toEqual(true);
  });
});
