import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import { stringToSortedArray } from '../static-functions';
import { Film } from '../film';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-add-new-film',
  templateUrl: './add-new-film.component.html',
  styleUrls: ['./add-new-film.component.scss']
})

export class AddNewFilmComponent implements OnInit {
  /**
   * alertType - INFO, ERROR, SUCCESS
   * serverMessage: message in alert after server response
   * filmData - data to bind with angular
   * inputForms - input props to create inputs with *ngFor
   */
  alertType: string;
  serverMessage: string;
  filmData = {
    title: null,
    year: null,
    critics: null,
    users: null,
    synopsis: null,
    plink: null,
    flink: null,
    actorlist: null
  };
  inputForms = [
    {label: 'Film title', id: 'title', type: 'text', placeholder: 'Film title'},
    {label: 'Year', id: 'year', type: 'number', placeholder: '1800-2018'},
    {label: 'Critics score', id: 'critics', type: 'number', placeholder: '0-100'},
    {label: 'Audience score', id: 'users', type: 'number', placeholder: '0-100'},
    {label: 'Poster link', id: 'plink', type: 'text', placeholder: 'https://images...jpg'},
    {label: 'Film link', id: 'flink', type: 'number', placeholder: 'XXXXXXX'},
    {label: 'Actors', id: 'actorlist', type: 'text', placeholder: 'FirstName LastName, ... ( 5 )'},
  ];

  constructor(private filmService: FilmService) { }

  ngOnInit() {
    this.alertType = 'info';
  }

  /**
   * Set test input to all forms
   */
  setTestInput() {
    this.filmData.title = 'FilmTitle';
    this.filmData.year = 1990;
    this.filmData.critics = 52;
    this.filmData.users = 100;
    this.filmData.synopsis = 'Some description here to get 200 characters for success: \n' +
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod ' +
      'tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ' +
      'At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea tak';
    this.filmData.plink = 'https://images-na.ssl-images-amazon.com/images/M/' +
      'MV5BY2Q2NzQ3ZDUtNWU5OC00Yjc0LThlYmEtNWM3NTFmM2JiY2VhXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR3,0,182,268_AL_.jpg';
    this.filmData.flink = 1234567;
    this.filmData.actorlist = 'FirstOne LastOnee, FirstTwo LastTwo, ' +
      'FirstThree LastThree, FirstFour LastFour, FirstFive LastFive';
  }

  /**
   * Submit to server => SUCCESS or ERROR alert
   */
  submitInput() {
    // go to the top of the page
    window.scrollTo(0, 0);

    // hide the alert first
    this.alertType = null;

    const actors = stringToSortedArray(this.filmData.actorlist);
    const data = new Film(
      this.filmData.title,
      this.filmData.year,
      {
        critics_score: this.filmData.critics,
        audience_score: this.filmData.users
      },
      this.filmData.synopsis,
      {
        original: this.filmData.plink
      },
      {
        imdb: this.filmData.flink
      },
      [
        { name: actors[0] },
        { name: actors[1] },
        { name: actors[2] },
        { name: actors[3] },
        { name: actors[4] }
      ]
    );

    // POST
    this.filmService.addNewFilm(JSON.stringify(data))
      .subscribe(film => {
          this.alertType = 'success';
          this.serverMessage = `Film ${film['title']}(${film['year']}) was successfully added to the collection`;
        },
        error => {
          this.alertType = 'error';
          this.serverMessage = error;
        });
  }
}
