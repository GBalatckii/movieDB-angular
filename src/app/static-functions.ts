/**
 * Check if input OK
 * @param {string} name - to define how to check input
 * @param {string} value - value
 * @returns {boolean} - SUCCESS ? TRUE : FALSE
 */
export function checkInput(name: string, value: string): boolean {
  if (!value || value.length === 0) {
    return false;
  }

  switch (name) {
    case 'title': {
      return value.length <= 50;
    }
    case 'desc': {
      return value.length >= 200;
    }
    case 'year': {
      const now = (new Date()).getFullYear();
      const valueInt = parseInt(value, 10);
      return (valueInt >= 1800 && valueInt <= now);
    }
    case 'critics':
    case 'users': {
      const valueInt = parseInt(value, 10);
      return (valueInt >= 0 && valueInt <= 100);
    }
    case 'flink': {
      const valueInt = parseInt(value, 10);
      return (valueInt >= 1 && valueInt <= 9999999);
    }
    case 'plink': {
      const regExp = /^https:\/\/images-na\.ssl-images-amazon\.com\/images\/M\/MV5B(\w+)@+\._V1_U(\w|,|_)+0,182,268_AL_\.jpg$/;
      return regExp.test(value);
    }
    case 'actorlist': {
      const actors = stringToSortedArray(value);
      return (actors.length === 5);
    }
    default: return false;
  }
}

/**
 * Clear the string to an array of actors
 * @param {string} str - string to sort
 * @returns {String[]} - Array of sorted items
 */
export function stringToSortedArray(str: string): string[] {
  return str
  // clear the string for wrong symbols
    .replace(/[^a-zA-Z\s,.\-_'0-9]+/g, '')
    // clear multiply spaces
    .replace(/\s\s+/g, ' ')
    // clear spaces near ","
    .replace(/ , |, | ,/g, ',')
    // clear commas at the beginning, at the end or multiplied
    .replace(/^,+|,+$/g, '')
    .replace(/,{2,}/g, ',')
    // trim and save to Array
    .trim()
    .split(',');
}
