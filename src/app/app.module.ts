import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { TopMoviesComponent } from './top-movies/top-movies.component';
import { ViewAllMoviesComponent } from './view-all-movies/view-all-movies.component';
import { AddNewFilmComponent } from './add-new-film/add-new-film.component';
import { ActorListComponent } from './actor-list/actor-list.component';
import { AppRoutingModule } from './/app-routing.module';
import { TopMoviesItemComponent } from './top-movies-item/top-movies-item.component';
import { FilmService } from './film.service';
import { ValidatorDirective } from './validator.directive';
import {environment} from '../environments/environment';

// import NbgModule.forRoot() to use angular-bootstrap
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    TopMoviesComponent,
    ViewAllMoviesComponent,
    AddNewFilmComponent,
    ActorListComponent,
    TopMoviesItemComponent,
    ValidatorDirective
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : [],
  ],
  providers: [
    FilmService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
