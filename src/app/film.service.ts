import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';

@Injectable()
export class FilmService {

  constructor(private http: HttpClient) { }

  /**
   * GET fetch all films
   * @returns {Observable<Object[]>} - [{title, year,...}, {}, ...]
   */
  getFilms(): Observable<Object[]> {
    return this.http.get<Object>('http://localhost:3001/loadfilms/')
      .map((res: Object) => <Object[]>res['movies'])
      .pipe(
        catchError(this.handleError)
      );
  }

  /**
   * GET fetch all actors
   * @returns {Observable<String[]>} - ['actor1', 'actor2', ...]
   */
  getActors(): Observable<String[]> {
    return this.http.get<String[]>(`http://localhost:3001/loadactors/`)
      .pipe(
        catchError(this.handleError)
      );
  }

  /**
   * GET fetch all films with this actor
   * @returns {Observable<Object[]>} - [{title, link}, {title, link}, ...]
   */
  getFilmsWithActor(name): Observable<Object[]> {
    return this.http.get<Object[]>(`http://localhost:3001/loadactorfilms/${name}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  /**
   * POST add new film
   * @param film - JSON with new film
   * @returns {Observable<Object>} - same as param if success
   */
  addNewFilm(film): Observable<Object> {
    return this.http.post<Object>(`http://localhost:3001/addnewfilm`, film, { headers: {'Content-Type': 'application/json'}})
      .pipe(
        catchError(this.handleError)
      );
  }

  /**
   * Sort the list of films
   * NOTE: not_stable_sort
   * @param films - Array of films to sort
   * @param sort - parameter
   */
  sortFilmList(films, sort) {
  return films.sort((a, b): number => {
      let objA;
      let objB;

      switch (sort) {
        case 'title':
          objA = b[sort];
          objB = a[sort];
          break;
        case 'year':
          objA = a[sort];
          objB = b[sort];
          break;
        case 'critics_score':
        case 'audience_score':
          objA = a.ratings[sort];
          objB = b.ratings[sort];
          break;
        default: break;
      }

      if (objA > objB) {
        return -1;
      } else if (objA < objB) {
        return 1;
      }
      return 0;
    });
  }

  // Error handler
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurrred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // return an ErrorObservable with a user-facing error message
    let message;

    switch (error.status) {
      case 0: {
        message = 'Server is down';
        break;
      }
      case 504:  {
        message = 'Network problem';
        break;
      }
      default: message = error.error;
    }

    return new ErrorObservable(message);
  }
}
