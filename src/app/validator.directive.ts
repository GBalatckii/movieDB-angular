import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, Validator } from '@angular/forms';
import { checkInput } from './static-functions';

@Directive({
  selector: '[appValidator][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: ValidatorDirective, multi: true }
  ]
})

export class ValidatorDirective implements Validator {
  // Id of form to check the input
  @Input('appValidator') inputId: string;

  validate(control: AbstractControl): {[key: string]: any} {
    return this.validateInput(this.inputId)(control);
  }

  private validateInput(name: string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
      const success = checkInput(name, control.value);
      return !success ? {'error': {value: control.value}} : null;
    };
  }
}
