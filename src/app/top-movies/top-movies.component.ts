import { Component, OnInit } from '@angular/core';
import { FilmService} from '../film.service';
import { Film } from '../film';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-top-movies',
  templateUrl: './top-movies.component.html',
  styleUrls: ['./top-movies.component.scss']
})

export class TopMoviesComponent implements OnInit {
  /**
   * films: sorted list of films
   * sortType: critics_score || audience_score
   * error: message from server or while connecting to it
   */
  films: Film[];
  sortType: string;
  error: string;

  constructor(private filmService: FilmService) { }

  ngOnInit() {
    this.sortType = 'critics_score';
    this.getFilms();
  }

  /**
   * Change the sort type
   * @param {string} arg - critics_score or audience_score
   */
  toggleSort(arg: string): void {
    this.sortType = arg;
    this.sortFilms(this.films);
  }

  /**
   * if no films sent or saved => error
   * sort the film list
   */
  sortFilms(films): void {
    if (!films && !this.films) {
      this.error = 'Nothing to show';
      return;
    }
    this.error = null;
    this.films = this.filmService.sortFilmList(films, this.sortType);
  }

  /**
   * GET films => filter with no synopsis or poster => save => sort
   */
  getFilms() {
    this.error = 'Connecting to server ...';
    this.films = null;
    this.filmService.getFilms()
     .map(films => films.filter((val => val['synopsis'] && val['posters'] && val['posters']['original'])))
     .subscribe(
       films => this.sortFilms(films),
       error => this.error = error);
  }

}
