import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopMoviesItemComponent } from './top-movies-item.component';

describe('TopMoviesItemComponent', () => {
  let component: TopMoviesItemComponent;
  let fixture: ComponentFixture<TopMoviesItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopMoviesItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopMoviesItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
