import { Component, Input, OnChanges } from '@angular/core';
import { Film} from '../film';

@Component({
  selector: 'app-top-movies-item',
  templateUrl: './top-movies-item.component.html',
  styleUrls: ['./top-movies-item.component.scss']
})

export class TopMoviesItemComponent implements OnChanges {

  /**
   * film: film from parent with all info
   * index: to dont hide first one
   * hide: toggle on click
   */
  @Input() film: Film;
  @Input() index: number;
  hide: boolean;

  constructor() { }

  /**
   * On init and if sort button pressed => default state: opened only the first one, other are closed
   */
  ngOnChanges() {
    this.hide = (this.index !== 0);
  }
}
