import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TopMoviesComponent} from './top-movies/top-movies.component';
import { ViewAllMoviesComponent } from './view-all-movies/view-all-movies.component';
import { AddNewFilmComponent} from './add-new-film/add-new-film.component';
import { ActorListComponent} from './actor-list/actor-list.component';

const routes: Routes = [
  { path: 'topfilms', component: TopMoviesComponent },
  { path: 'viewallmovies', component: ViewAllMoviesComponent },
  { path: 'addnewfilm', component: AddNewFilmComponent },
  { path: 'actors', component: ActorListComponent },
  { path: '', redirectTo: '/topfilms', pathMatch: 'full' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
