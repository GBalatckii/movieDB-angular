export class Film {
  constructor(
    public title: string,
    public year: number,
    public ratings: {
      critics_score: number
      audience_score: number
    },
    public synopsis: string,
    public posters: {
      original: string
    },
    public alternate_ids: {
      imdb: number
    },
    public abridged_cast: [
        { name: string },
        { name: string },
        { name: string },
        { name: string },
        { name: string }
      ]
  ) {  }
}
