import { Component, OnInit } from '@angular/core';
import { FilmService} from '../film.service';
import { Film} from '../film';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-view-all-movies',
  templateUrl: './view-all-movies.component.html',
  styleUrls: ['./view-all-movies.component.scss']
})

export class ViewAllMoviesComponent implements OnInit {
  /**
   * films - sorted list of films
   * sortType - sort the list (title, year, critics_score, audience_score)
   * hideActors - array with a value for each row ( TRUE = hide)
   * error - error from server or a message while connecting
   */
  films: Film[];
  sortType: string;
  hideActors: boolean[];
  error: string;

  constructor(private filmService: FilmService) { }

  ngOnInit() {
    this.sortType = 'title';
    this.getFilms();
  }

  /**
   * Change the sort type
   * @param {string} arg - title, year, critics_score, audience_score
   */
  toggleSort(arg: string): void {
    this.sortType = arg;
    this.sortFilms(this.films);
  }

  /**
   * if no films sent or saved => error
   * toggleRow with index -1 to close all rows
   * sort the film list
   */
  sortFilms(films): void {
    if (!films && !this.films) {
      this.error = 'Nothing to show';
      return;
    }
    this.toggleRow(-1);
    this.error = null;
    this.films = this.filmService.sortFilmList(films, this.sortType);
  }

  /**
   * Close all hidden actor rows, toggle the called one
   * @param {number} i - row number
   */
  toggleRow(i: number) {
    this.hideActors = this.hideActors.map((val, index) => {
      if (index === i) {
        return !val;
      }
      return true;
    });
  }

  /**
   * Set the new length of table to hide all rows after the GET request
   * @param {number} len - new length
   */
  setNewLength(len: number) {
    const newArr = [];
    for (let i = 0; i < len; i += 1) { newArr.push(true); }
    this.hideActors = newArr;
  }

  /**
   * GET films => save => set hide length => sort
   */
  getFilms() {
    this.error = 'Connecting to server ...';
    this.films = null;
    this.filmService.getFilms()
      .map(films => {
        this.setNewLength(films.length);
        return films;
      })
      .subscribe(
        films => this.sortFilms(films),
        error => this.error = error);
  }
}
